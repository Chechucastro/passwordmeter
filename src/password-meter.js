(function ($) {
    'use strict';
    var pluginName = 'passwordMeter';
    $[pluginName] = (function () {
        /**
         * Plugin Constructor. This function build the basic object for the plugin
         * @param (object) element - The jQuery or Zepto DOM element
         * @param (object) options - A list of options for the plugin
         */
        $[pluginName] = function (element, options) {
            this.options = $.extend({}, options);
            // Params
            this.$username = $(this.options.username);
            this.$password = $(element);
            this.$meterbar = $(this.options.meterbar);

            // Translations
            this.$similar2User = this.options.similar_to_username;
            this.$tooShort     = this.options.too_short;
            this.$very_weak    = this.options.very_weak;
            this.$weak         = this.options.weak;
            this.$good         = this.options.good;
            this.$strong       = this.options.strong;

            // Css classes
            this.$css2Short    = "too_short";
            this.$cssVeryWeak  = "very_weak";
            this.$cssWeak      = "weak";
            this.$cssGood      = "good";
            this.$cssStrong    = "strong";
            this.$css2Short    = "too_short";
            // Css meter bar & message trads
            this.$cssMeterBar  = "password-meter-bar";
            this.$cssMeterMsg  = "password-meter-message";

            // init plugin
            return this.initMeter();
        };

        $[pluginName].prototype = {
            initMeter: function () {
                var that = this;
                this.$password.on('change keyup', function (e) {
                    that.passwordRating($(this).val());
                });
            },
            passwordRating: function (password) {
                var that = this, cssClass;
                var pass = password.length;
                if (pass >= 0 && pass <= 2){     cssClass   = this.$css2Short;  }
                else if(pass >= 3 && pass <= 4){ cssClass   = this.$cssVeryWeak;}
                else if(pass >= 5 && pass <= 7){ cssClass   = this.$cssWeak;    }
                else if(pass >= 8 && pass <= 9){ cssClass   = this.$cssGood;    }
                else if(pass >= 10){             cssClass   = this.$cssStrong;  }
                else if(!pass){                  cssClass   = this.$css2Short;  }

                // Update meter..
                that.updateMeter(cssClass);
            },
            updateMeter: function (cssClass) {
                var messages = {
                    "similar_to_username":  this.$similar2User,
                    "too_short":            this.$tooShort,
                    "very_weak":            this.$very_weak,
                    "weak":                 this.$weak,
                    "good":                 this.$good,
                    "strong":               this.$strong
                }

                // Meter bar
                $('.'+this.$cssMeterBar).attr('class', ''+this.$cssMeterBar+' password-meter_' + cssClass + '');
                // Text message
                $('.'+this.$cssMeterMsg)
                    .attr('class', ''+this.$cssMeterMsg+' password-meter-message_'+ cssClass+ ' box-flt-l font-level5')
                    .text(messages[cssClass]);
            }
        };
        // Building the plugin
        /**
         * The plugin component
         * @param  {object} options - list of all parameters for the jQuery/Zepto module
         * @return {object} - The jQuery/Zepto DOM element
         */
        return $[pluginName];
    }(window));
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$(this).data(pluginName)) {
                if (options === 'destroy') {
                    return;
                }
                $(this).data(pluginName, new $[pluginName](this, options));
            } else {
                var $plugin = $(this).data(pluginName);
            }
        });
    };
})(window.Zepto || window.jQuery);
