passwordMeter
================================

Zepto and jQuery Plugin to find password security level

Options :
---------
meterbar               Default : true        Type : string          Mandatory css class where the plugin builds the meter

// Translations sent as parameters in order to display messages in different languages

similar_to_username    Default : true         Type : string       Message when password is similar to user name input
too_short              Default : true         Type : string       Message when password is too short. Between 0-2 chars.
very_weak              Default : true         Type : string       Message when password is very weak or between 3 and 5 chars.
weak                   Default : true         Type : string       Message when password is weak or between 5 and 7 chars.
good                   Default : true         Type : string       Message when password lenght is between 8 and 9 chars.
strong                 Default : true         Type : string       Message when password lenght is higher than 10 chars.
